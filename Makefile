#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2016/08/18 18:45:23 by pmartin          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME = ft_ls
CC = cc
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
LIBFT = libft/
SRC := $(filter %.c, $(shell find src -type f))
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT)
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re