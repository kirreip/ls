/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 00:51:59 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 15:09:16 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_atoi(const char *s)
{
	int		i;
	int		nb;
	int		cond;

	i = 0;
	nb = 0;
	cond = 0;
	while ((int)s[i] == '\r' || (int)s[i] == '\f'
		|| (int)s[i] == '\t' || (int)s[i] == '\v'
		|| (int)s[i] == '\n' || (int)s[i] == ' ')
		i++;
	if (s[i] == '-' || s[i] == '+')
		if (s[i++] == '-')
			cond = 1;
	while (ft_isdigit(s[i]))
		nb = nb * 10 + s[i++] - 48;
	return ((cond == 1) ? nb * -1 : nb);
}
