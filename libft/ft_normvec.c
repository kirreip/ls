/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_normvec.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 15:18:50 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 16:05:58 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double		ft_normvec(t_vec *vec)
{
	double	len;

	len = sqrt(ft_sq(vec->x) + ft_sq(vec->y) + ft_sq(vec->z));
	vec->x /= len;
	vec->y /= len;
	vec->z /= len;
	return (len);
}
