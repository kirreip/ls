/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/09 01:11:05 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 13:13:08 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t	i;
	char	*str;

	i = 0;
	if (!(str = ft_strnew(ft_strlen(s))))
		return (0);
	if (!s || !f)
		return (0);
	while ((unsigned char)s[i])
	{
		str[i] = f(i, s[i]);
		i++;
	}
	str[i + 1] = f(i + 1, s[i + 1]);
	str[i + 2] = '\0';
	return (str);
}
