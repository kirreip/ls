/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 18:35:32 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/08 20:01:21 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef		LS_H
# define	LS_H
# include "../libft/includes/libft.h"
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <time.h>
# include <unistd.h>
# include <stdlib.h>
# include <errno.h>
# include <stdio.h>
# include <pwd.h>
# include <sys/types.h>
# include <grp.h>

typedef				struct s_form
{
	int				uid;
	int				gid;
	int				size;
	int				time;
	int				nlink;
}					t_form;
typedef				struct s_data
{
	char			*size;
	int				ntm;
	int				mode;
	char			*gid;
	char			*uid;
	char			*time;
	char			*nlink;
}					t_data;

typedef				struct s_listd
{
	char			*name;
	char			*pn;
	t_data			data;
	struct s_listd	*nxt;
	struct s_listd	*prs;
}					t_listd;

typedef				struct s_var
{
	int				l;
	int				a;
	int				pr;
	int				gr;
	int				t;
	char			*buf;
	t_form			form;
}					t_var;

int			ft_checkp(char *s);
int			parser(t_var *var, int ac, char **av);
void		init_var(t_var *var);
void		ft_putendls(char *s);
void		search_simple(t_var *var, char *dir, int gr);
void		print_list(t_var *var, t_listd *elem);
void		free_list(t_listd *list);
t_listd		*new_elem(t_listd *elem, struct stat *buf, char *s, t_var *var);
t_listd		*first_elem(struct stat *buf, char *s, t_var *var);
char		*ft_strdupcat(char *s1, char *s2);
void		printlistl(t_var *var, t_listd *elem);
void		fill_data(t_var *var,t_listd *elem, struct stat *buf);
void		reset_size(t_var *var);
#endif










