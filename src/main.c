/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 18:44:29 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/10 16:53:08 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

/*static int	nb_indirect(char **av)
{
	int i;

	i = 1;
	while (av[i] && av[i][0] == '-')
		i++;
	printf("I == %d\n", i);
	return (i - 1);
	}*/

int			main(int ac, char **av)
{
	t_var var;
	int i;

	i = 0;
	init_var(&var);
	if ((i = parser(&var, ac, av)) && (ac - i) > 2)
	{
		printf("ac == %d ||i == %d\n", ac, i);
		while (av[i])
			search_simple(&var, av[(i++)], 0);
	}
	else if (!(ac - 1 == i))
		search_simple(&var, ".", 0);
	else
		while (av[i])
			search_simple(&var, av[(i++)], 0);
	//printf("a%d r%d R%d t%d l%d\n", var.a, var.pr, var.gr, var.t, var.l);
	//printf("%s", ft_strdupcat("..", "."));
	//Dir = AV
}
