/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdupcat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/19 11:11:43 by pmartin           #+#    #+#             */
/*   Updated: 2016/08/19 11:24:06 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	ft_putendls(char *s)
{
	ft_putstr(s);
	ft_putstr(":\n");
}

char	*ft_strdupcat(char *s1, char *s2)
{
	char *str;

	if (!(str = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 2)))
		return (0);
	str = ft_strcpy(str, s1);
	str[ft_strlen(s1)] = '/';
	str = ft_strcat(str, s2);
	return (str);
}
