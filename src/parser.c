/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 19:06:30 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/10 15:15:36 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int		ft_strch(char *s, char c)
{
	while (*s)
		if (*s++ == c)
			return (1);
	return (0);
}

int		parser(t_var *var, int ac, char **av)
{
	int i;

	i = 1;
	while (av[i] && av[i][0] == '-' && av[i][1] != '-' && ac--)
	{
		if (ft_strch(av[i], 'a'))
			var->a = 1;
		if (ft_strch(av[i], 'R'))
			var->gr = 1;
		if (ft_strch(av[i], 'r'))
			var->pr = 1;
		if (ft_strch(av[i], 't'))
			var->t = 1;
		if (ft_strch(av[i], 'l'))
			var->l = 1;
		i++;
	}
	return (i);
}
