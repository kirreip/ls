/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/16 11:41:39 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/08 20:02:16 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	free_list(t_listd *elem)
{
	t_listd *tmp;

	while (elem)
	{
		free(elem->name);
		tmp = elem->nxt;
		free(elem);
		elem = tmp;
	}
}

int		ft_checkp(char *s)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (s[i] == '.' && (i < 0 || s[i - 1] == '/'))
			return (1);
		i++;
	}
	return (0);
}

void	print_list(t_var *var, t_listd *elem)
{
	while ((elem = elem->nxt))
		(var->l) ? printlistl(var, elem) : ft_putendl(elem->name);
}

t_listd	*new_elem(t_listd *elem, struct stat *buf, char *s, t_var *var)
{
	//printf("NEW Elem\n");
	if (!((*elem).nxt = (t_listd *)malloc(sizeof(t_listd))))
		return (0);
	fill_data(var, elem->nxt, buf);
	//ft_memcpy(&elem->nxt->data, buf, sizeof(struct stat));
	elem->nxt->name = ft_strdup(s);
	elem->nxt->pn = var->buf;
	(*elem).nxt->prs = elem;
	(*elem).nxt->nxt = 0;
	return ((*elem).nxt);
}

t_listd	*first_elem(struct stat *buf, char *s, t_var *var)
{
	t_listd *elem;
//	static t_data empty;

	//printf("First Elem\n");
	if (!(elem = (t_listd *)malloc(sizeof(t_listd))))
		return (0);
	fill_data(var, elem, buf);
	elem->name = ft_strdup(s);
	elem->pn = var->buf;
	elem->nxt = 0;
	elem->prs = 0;
	return (elem);
}
