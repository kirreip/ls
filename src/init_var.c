/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_var.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 18:57:59 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/10 15:21:24 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

char	*strcatnxt(char *m, int d, int h, int y)
{
	char *s;
	char *tmp;

	if (y)
		;
	s = ft_strnew(20);
	ft_strcpy(s, m);
	tmp = ft_itoa(d);
	ft_strcpy(s + 4, " ");
	ft_strcpy(s + 5, tmp);
	free(tmp);
	tmp = ft_itoa(h % 256);
	ft_strcpy(s + 6, " ");
	ft_strcpy(s + 7, tmp);
	h = h >> 8;
	free(tmp);
	tmp = ft_itoa(h);
	ft_strcpy(s + 6, ":");
	ft_strcpy(s + 7, tmp);
	free(tmp);
	return (s);
}

void	reset_size(t_var *var)
{
	var->form.uid = 0;
	var->form.gid = 0;
	var->form.time = 0;
	var->form.size = 0;
	var->form.nlink = 0;
}

char	*get_month(int d, int y)
{
	if (d < 31)
		return (ft_strdup("Jan"));
	else if (d < 59 || (!(y % 4) && d < 60))
		return (ft_strdup("Feb"));
	else if (d < 90 || (!(y % 4) && d < 91))
		return (ft_strdup("Mar"));
	else if (d < 120 || (!(y % 4) && d < 121))
		return (ft_strdup("Apr"));
	else if (d < 151 || (!(y % 4) && d < 152))
		return (ft_strdup("May"));
	else if (d < 181 || (!(y % 4) && d < 182))
		return (ft_strdup("June"));
	else if (d < 212 || (!(y % 4) && d < 213))
		return (ft_strdup("Jul"));
	else if (d < 242 || (!(y % 4) && d < 243))
		return (ft_strdup("Aug"));
	else if (d < 273 || (!(y % 4) && d < 274))
		return (ft_strdup("Sep"));
	else if (d < 303 || (!(y % 4) && d < 304))
		return (ft_strdup("Oct"));
	else if (d < 334 || (!(y % 4) && d < 335))
		return (ft_strdup("Nov"));
	else
		return (ft_strdup("Dec"));
}

char	*fill_time(time_t time)
{
	int		y;
	char	*m;
	int		d;
	int		h;
	char	*s;

	y = time / 31557600 + 1970;
	d = (time % 31557600) / 86400;
	m = get_month(d, y);
	h = ((time % 31557600) % 86400) % 3600;
/*	h = h << 8;
	h += ((time % 31557600) % 86400) / 3600;*/
	s = strcatnxt(m, d, h, y);
	//printf("Y %d, M %s, D %d, H %d\n", y, m, d, h);
	free(m);
	return (s);
}

void	fill_data(t_var *var, t_listd *elem, struct stat *buf)
{
	struct passwd	*passent;
	struct group	*grp;
	int				tmp;

	elem->data.nlink = ft_itoa((int)buf->st_nlink);
	if ((tmp = ft_strlen(elem->data.nlink)) > var->form.nlink)
		var->form.nlink = tmp;
	passent = getpwuid(buf->st_uid);
	elem->data.uid = ft_strdup(passent->pw_name);
	if ((tmp = ft_strlen(elem->data.uid)) > var->form.uid)
		var->form.uid = tmp;
	grp = getgrgid(buf->st_gid);
	elem->data.gid = ft_strdup(grp->gr_name);
	if ((tmp = ft_strlen(elem->data.gid)) > var->form.gid)
		var->form.gid = tmp;
	elem->data.size = ft_itoa((int)buf->st_size);
	if ((tmp = ft_strlen(elem->data.size)) > var->form.size)
		var->form.size = tmp;
	elem->data.mode = (int)buf->st_mode;
	elem->data.time = fill_time(buf->st_mtime);
	if ((tmp = ft_strlen(elem->data.time)) > var->form.time)
		var->form.time = tmp;
}

void	init_var(t_var *var)
{
	errno = 0;
	var->l = 0;
	var->gr = 0;
	var->a = 0;
	var->pr = 0;
	var->t = 0;
}
