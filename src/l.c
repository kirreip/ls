/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 14:28:45 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/09 16:27:51 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	printspace(int nb)
{
	while (nb--)
		ft_putchar(32);
}

void	printtype(t_listd *elem)
{
	if ((elem->data.mode & S_IFMT) == S_IFSOCK)
		ft_putchar('@');
	else if ((elem->data.mode & S_IFMT) == S_IFLNK)
		ft_putchar('&');
	else if ((elem->data.mode & S_IFMT) == S_IFREG)
		ft_putchar('-');
	else if ((elem->data.mode & S_IFMT) == S_IFDIR)
		ft_putchar('d');
	else if ((elem->data.mode & S_IFMT) == S_IFCHR)
		ft_putchar('|');
	else if ((elem->data.mode & S_IFMT) == S_IFIFO)
		ft_putchar('F');
}

void	printlistl(t_var *var, t_listd *elem)
{
	printtype(elem);
	ft_putchar((elem->data.mode & S_IRUSR) ? 'r' : '-');
	ft_putchar((elem->data.mode & S_IWUSR) ? 'w' : '-');
	ft_putchar((elem->data.mode & S_IXUSR) ? 'x' : '-');
	ft_putchar((elem->data.mode & S_IRGRP) ? 'r' : '-');
	ft_putchar((elem->data.mode & S_IWGRP) ? 'w' : '-');
	ft_putchar((elem->data.mode & S_IXGRP) ? 'x' : '-');
	ft_putchar((elem->data.mode & S_IROTH) ? 'r' : '-');
	ft_putchar((elem->data.mode & S_IWOTH) ? 'w' : '-');
	ft_putchar((elem->data.mode & S_IXOTH) ? 'x' : '-');
	printspace(var->form.nlink - ft_strlen(elem->data.nlink) + 2);
	ft_putstr(elem->data.nlink);
	printspace(var->form.uid - ft_strlen(elem->data.uid) + 1);
	ft_putstr(elem->data.uid);
	printspace(var->form.gid - ft_strlen(elem->data.gid) + 2);
	ft_putstr(elem->data.gid);
	printspace(var->form.size - ft_strlen(elem->data.size) + 1);
	ft_putstr(elem->data.size);
	printspace(var->form.time - ft_strlen(elem->data.time) + 1);
	ft_putstr(elem->data.time);
	printspace(1);
	ft_putendl(elem->name);
}
