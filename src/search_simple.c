/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_simple.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/18 19:46:56 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/08 19:59:53 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void	swap_elem(t_listd *elem1, t_listd *elem2, t_listd **begin)
{
	if (elem1->prs)
		elem1->prs->nxt = elem2;
	else
		*begin = elem2;
	if (elem2->nxt)
		elem2->nxt->prs = elem1;
	elem1->nxt = elem2->nxt;
	elem2->prs = elem1->prs;
	elem2->nxt = elem1;
	elem1->prs = elem2;
}

int		ft_strcmp2(const char *s1, const char *s2)
{
	int i;

	i = 0;
	while (s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	return (s2[i] - s1[i]);
}

void	sort_list(t_listd **elem, int (*strcmp)(const char *s1, const char *s2))
{
	int		o;
	t_listd	*tmp;

	o = 1;
	while (o)
	{
		o = 0;
		tmp = *elem;
		while ((tmp = tmp->nxt) && tmp->nxt)
			if ((strcmp(tmp->name, tmp->nxt->name) > 0) && (o = 1))
				swap_elem(tmp, tmp->nxt, elem);
	}
}

void	search_simple(t_var *var, char *dir, int gr)
{
	DIR				*fd;
	struct dirent	*fold;
	struct stat		buf;
	t_listd			*list;
	t_listd			*beg;

	list = 0;
	reset_size(var);
	if (gr && (!ft_checkp(dir) || (ft_checkp(dir) && var->a)))
	{
		ft_putchar('\n');
		ft_putendls(dir);
	}
	fd = opendir(dir);
	while (!errno && (fold = readdir(fd)))
		if (fold->d_name[0] != '.' || (var->a))
		{
			stat((var->buf = ft_strdupcat(dir, fold->d_name)), &buf);
			//printf("Name |%s| Pass|%hu|\n", fold->d_name, buf.st_mode);
			if (!list && (list = first_elem(&buf, fold->d_name, var)))
				beg = list;
			else
				list = new_elem(list, &buf, fold->d_name, var);
		}
	closedir(fd);
	sort_list(&beg, ((var->pr) ? &ft_strcmp2 : &ft_strcmp));
	print_list(var, beg);
	list = beg;
	if (var->gr)
		while (!errno && list)
		{
			stat(list->pn, &buf);
			if (S_ISDIR(buf.st_mode) && ft_strcmp(list->name, ".")
				&& ft_strcmp(list->name, ".."))
				search_simple(var, list->pn, 1);
			list = list->nxt;
		}
	free_list(beg);
}
